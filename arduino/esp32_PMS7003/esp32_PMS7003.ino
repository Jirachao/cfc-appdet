
#include "PMS.h"
#include "BluetoothSerial.h"

BluetoothSerial ESP_BT;
PMS pms(Serial);
PMS::DATA data;

// this section for dimming status led (optional)-----
const int freq = 5000;
const int ledChannel = 0;   
const int resolution = 8;
const int ledPin = 0; 
// end dimming status led (optional)-----

String value;
String message;

bool deviceConnected = false;

// callback section for BL to trigger LED status
void callback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  if(event == ESP_SPP_SRV_OPEN_EVT){
    deviceConnected = true;
    Serial.println("Client Connected");
  }

  if(event == ESP_SPP_CLOSE_EVT ){
    deviceConnected = false;
    Serial.println("Client disconnected");
  }
}

// setup
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);         //setting baudrate

  ESP_BT.register_callback(callback);     //register Bluetooth callback

  if(!ESP_BT.begin("PMS7003")){
    Serial.println("An error occurred initializing Bluetooth");
  }else{
    Serial.println("Bluetooth initialized");
  }
  
  //setup status LED
  ledcSetup(ledChannel, freq, resolution);          
  ledcAttachPin(ledPin, ledChannel);ledcSetup(ledChannel, freq, resolution);
  ledcAttachPin(ledPin, ledChannel);
  
}

void loop() {
  
   if (pms.read(data)){
      message = data.PM_AE_UG_2_5;
      if(deviceConnected){    
        ledcWrite(ledChannel, 50);    //if device connected, stady status led
        value = ESP_BT.readString();
        if(value == "S"){             // check input serial for "S" signal to retrieve specific pollution data (currently returning pm2.5)
          Serial.println(value);      
          ESP_BT.print(message);
        }else
        {
          value = ".";
          Serial.print(value);
        }
      }
      else
      { // if device not connected, led blick
        for(int dutyCycle = 0; dutyCycle <= 100; dutyCycle++){
          // changing the LED brightness with PWM
          ledcWrite(ledChannel, dutyCycle);
          delay(5); 
        }
        for(int dutyCycle = 100; dutyCycle >= 0; dutyCycle--){
          // changing the LED brightness with PWM
          ledcWrite(ledChannel, dutyCycle);
          delay(5);
        }
      }
   }
   
  
}

String readDustSensor(){
    String dustDensity = "0";
    if (pms.read(data)){          // read data from sensor
      dustDensity = String(data.PM_AE_UG_2_5);    //extract pm2.5 data only
      Serial.println(data.PM_AE_UG_2_5);
    }
    return dustDensity;
    
}
