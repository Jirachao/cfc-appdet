import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { YourLocationTabPageRoutingModule } from './your-location-tab-routing.module';

import { ComponentsModule } from '../components/component.module';
import { YourLocationTabPage } from './your-location-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    YourLocationTabPageRoutingModule,
    ComponentsModule
  ],
  declarations: [YourLocationTabPage]
})
export class YourLocationTabPageModule {}
