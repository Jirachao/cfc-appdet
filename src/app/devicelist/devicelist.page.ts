import { Component, OnInit } from '@angular/core';
import { NavController,ModalController,AlertController, ToastController } from '@ionic/angular';
import { RestapiService } from '../services/restapi.service';
import { StorageService } from '../services/storage.service';
import { BluetoothService } from '../services/bluetooth.service';
import { Device } from '@ionic-native/device/ngx';
import { PushnotificationService } from '../services/pushnotification.service';

declare var cordova: any;
@Component({
  selector: 'app-devicelist',
  templateUrl: './devicelist.page.html',
  styleUrls: ['./devicelist.page.scss'],
})
export class DevicelistPage implements OnInit {
  pairedList: pairedlist;
  listToggle: boolean = false;
  pairedDeviceID: number = 0;
  selectedFilter: boolean = false;
  deviceReg : any;

  constructor(private nav:NavController,private modalCtrl:ModalController,public alertController: AlertController, public restProvider: RestapiService,public storage: StorageService, private toastCtrl: ToastController, private bluetooth: BluetoothService, private device: Device,private pushnotification : PushnotificationService) { 
    this.selectedFilter = false;
    this.deviceReg = {
      'deviceId': this.device.uuid,
      'authToken': 'myauthtoken',
      'pushToken': ''
    };
    this.checkBluetoothEnabled();
    
  }

  ngOnInit() {
  }

  dismiss() {
    
    this.modalCtrl.dismiss({
      'dismissed': this.selectedFilter
    });
  }

  selectDevice(index) {
    let connectedDevice = this.pairedList[index];
    if (!connectedDevice.address) {
      this.showError('Select Paired Device to connect');
      return;
    }
    this.bluetooth.connect(connectedDevice).then((result:boolean) => {
      
      if (result){
        this.bluetooth.listToggle = true;
        this.listToggle = true;
        this.storage.loadStorage('pushnotification').then((token) => {
          this.deviceReg.pushToken = token
          this.registerDevice(connectedDevice);
        }, (err) => {

        });
        
      }else{
        this.showToast("Unsuccessfully Connected");
        
      }
      
    }, (err) => {
      this.showToast("Unsuccessfully Connected");
      /*this.storage.clearStorage().then((result:boolean) => {
        if (result){
          //alert(result);
        }
      }, (err) => {
        console.log(err);
      });*/
    });
  }

  onClickDevice(index){
    this.bluetooth.checkBluetoothEnabled().then((result:boolean) => {
        this.listToggle = this.bluetooth.listToggle;
         this.presentAlertConfirm(index);
    }, (err) => {
      this.listToggle = this.bluetooth.listToggle;
      this.showToast("Please Enable Bluetooth")
    });
  }

  async presentAlertConfirm(index) {
    let connectedDevice = this.pairedList[index];
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Please confirm to add '+connectedDevice.name,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirm',
          handler: () => {
            console.log('Confirm Okay');
            this.selectDevice(index);
            
          }
        }
      ]
    });

    await alert.present();
  }

  registerDevice(connectedDevice) {
    console.log("request json >>"+JSON.stringify(this.deviceReg));
    this.restProvider.registerDevice(this.deviceReg).then((result) => {
      console.log(result);
      let res : any;
      res = result;
      let clientId = JSON.parse(res.data);
      if (clientId.clientId!=undefined){
        connectedDevice.clientId = clientId.clientId;
        connectedDevice.deviceId = this.deviceReg.deviceId;
      }
      
      this.storage.saveStorage('sensor',connectedDevice).then((result:boolean) => {
        if (result){
          this.showToast("Successfully Add Device");
          this.selectedFilter = true;
          this.dismiss();
        }
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

  checkBluetoothEnabled() {
    this.bluetooth.checkBluetoothEnabled().then((result:boolean) => {
      if (result){  
        this.listToggle = this.bluetooth.listToggle;
        this.listPairedDevices();
      }
    }, (err) => {
      this.listToggle = this.bluetooth.listToggle;
      this.showToast("Please Enable Bluetooth")
    });
  }

  listPairedDevices() {
    this.bluetooth.listPairedDevices().then((result:any) => {
      this.pairedList = result;
      this.listToggle = this.bluetooth.listToggle;
      
    }, (err) => {
      this.showError("Lis Device not found");
      this.listToggle = this.bluetooth.listToggle;
    });
  }
  
  showError(error) {
    alert(JSON.stringify(error));
  }

  async showToast(msj) {
    const toast = await this.toastCtrl.create({
      message: msj,
      duration: 1000
    });
    toast.present();

  }

}

interface pairedlist {
  "class": number,
  "id": string,
  "address": string,
  "name": string
}
