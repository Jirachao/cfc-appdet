import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevicelistPage } from './devicelist.page';

describe('DevicelistPage', () => {
  let component: DevicelistPage;
  let fixture: ComponentFixture<DevicelistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicelistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevicelistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
