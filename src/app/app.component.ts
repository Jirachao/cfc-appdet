import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { StorageService } from './services/storage.service';
import { PushnotificationService } from './services/pushnotification.service';
import { RestapiService } from './services/restapi.service';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  deviceReg : any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public storage: StorageService,
    private pushnotification : PushnotificationService,
    public restProvider: RestapiService,
    private device: Device
  ) {
    this.initializeApp();
  }
  
  initializeApp() {
    
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.deviceReg = {
        'deviceId': this.device.uuid,
        'authToken': 'myauthtoken',
        'pushToken': ''
      };
      //this.loadDatastorage();
      this.generatePushToken();
      this.splashScreen.hide();
    });
  }

  /*loadDatastorage(){
    this.storage.loadStorage('pushnotification').then((result) => {
      //let res:any;
      console.log("success>>"+result);
      if (result==null||result==undefined){
        this.generatePushToken();
      }
    }, (err) => {
      
      console.log("error>>"+err);
    });
  }*/

  generatePushToken(){
    this.pushnotification.initializeFirebaseAndroid().then((token) => {
      this.storage.saveStorage('pushnotification',token).then((result:boolean) => {
        if (result){
          console.log("success>>"+token);
          this.deviceReg.pushToken = token;
          this.restProvider.registerDevice(this.deviceReg).then((result) => {
            console.log(result);
            let res : any;
            res = result;
          }, (err) => {
            console.log(err);
          });
        }
        
      }, (err) => {
        console.log(err);
      });
    }, (err) => {

    });
  }
}
