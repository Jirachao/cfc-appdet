import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { KnowledgeTabPage } from './KnowledgeTab.page';
import { ComponentsModule } from '../components/component.module';

import { KnowledgeTabPageRoutingModule } from './KnowledgeTab-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule,
    KnowledgeTabPageRoutingModule
  ],
  declarations: [KnowledgeTabPage]
})
export class KnowledgeTabPageModule {}
