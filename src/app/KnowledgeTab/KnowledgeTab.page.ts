import { Component } from '@angular/core';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser/ngx';


@Component({
  selector: 'app-KnowledgeTab',
  templateUrl: 'KnowledgeTab.page.html',
  styleUrls: ['KnowledgeTab.page.scss']
})
export class KnowledgeTabPage {
  bannerInformation = "../../assets/banner/banner-01.jpg";
  links: any[] = [
    {
      id: 1,
      url: 'https://www.who.int/health-topics/air-pollution#tab=tab_1',
      text: 'WHO Air Pollution Information',
      detailInformation:'tab1.png'
    },
    {
      id: 2,
      url: 'https://www.bangkokhospital.com/en/disease-treatment/PM2.5-dust-and-the-elderly',
      text: 'Impact of PM2.5 on the Elderly',
      detailInformation:'tab2.png'
    },
    {
      id: 3,
      url: 'https://www.samitivejhospitals.com/en/pm2-5-children-health/',
      text: 'PM2.5 Tiny Pollutants and their Effect on Children Health',
      detailInformation:'tab3.png'
    }
  ];

  constructor(private themeableBrowser: ThemeableBrowser) {}
  
  openWeb(url,info){
    
    const options: ThemeableBrowserOptions = {
      statusbar: {
        color: '#81a8f1'
      },
      toolbar: {
        height: 0,
        color: '#81a8f1'
      },
      title: {
        color: '#ffffffff',
        showPageTitle: true,
        staticText: 'Knowledge Information'
      },
      closeButton: {
        wwwImage: '../../assets/webView/close.png',
        wwwImagePressed: '../../assets/webView/close.png',
        align: 'right',
        event: 'closePressed'
      },
    };

    const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);
   
}

}
