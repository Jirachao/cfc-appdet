import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KnowledgeTabPage } from './KnowledgeTab.page';

const routes: Routes = [
  {
    path: '',
    component: KnowledgeTabPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KnowledgeTabPageRoutingModule {}
