import { Component, NgZone, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PolserviceService } from '../services/polservice.service'
import { GeolocationService } from '../services/geolocation.service';

import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-ReportTab',
  templateUrl: 'ReportTab.page.html',
  styleUrls: ['ReportTab.page.scss']
})
export class ReportTabPage implements OnInit {
  polDesc: String;
  polType: String;
  b64Image: any;
  firstTakePhoto: boolean = true;
  timestamp: String;
  currentLocation = { lat: 0, lng: 0 };
  loading: any;
  causes: any[] = [

    {
      id: 1,
      problem: 'Black Smoke Car'
    },
    {
      id: 2,
      problem: 'Burning'
    },
    {
      id: 3,
      problem: 'Construction Dust'
    },
    {
      id: 4,
      problem: 'Factory Toxic fumes'
    },
    {
      id: 5,
      problem: 'Wildfire'
    }
  ];
  polservice: any;
  form = new FormGroup({
    polType: new FormControl('', Validators.required),
    polDesc: new FormControl('', Validators.required),
  });
  constructor(private geolocation: GeolocationService, public alertCtrl: AlertController, private camera: Camera, public loadingCtrl: LoadingController, private ngZone: NgZone, public polserviceService: PolserviceService) {
  }
  ngOnInit() {
  }
  clearData() {
    console.log("clearData");
    this.b64Image = undefined;
    this.form.setValue({ polType: '', polDesc: '' });
    this.firstTakePhoto = true;
    this.timestamp = "";
    this.currentLocation = { lat: 0, lng: 0 };
    console.log("1:" + this.b64Image + " |2" + this.form.value.polDesc + " |3" + this.form.value.polType + "  |4" + this.firstTakePhoto + "  |5" + this.timestamp + "  |6" + this.currentLocation);
  }
  async takePhoto() {
    this.loading = await this.loadingCtrl.create({
      spinner: null,
      message: "<img  src='../../../assets/icon/loading.gif'>",
      // duration: 5000,
      translucent: true,
      cssClass: 'custom-loading',
    });
    this.loading.present();
    let options = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 500,
      targetHeight: 500,
      quality: 100,
      allowEdit: false,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    this.camera.getPicture(options).then((imageData) => {
      this.b64Image = "data:image/jpeg;base64," + imageData;
      this.firstTakePhoto = false;
    })
      .catch(err => {
        console.log(err);
      })

    this.loading.dismiss();

  }
  async getLocation() {
    return new Promise(async (resolve, reject) => {
      try {
        console.log("getLocation");
        this.geolocation.getLatLon().then((location: any) => {
          this.currentLocation.lat = location.latitude;
          this.currentLocation.lng = location.longitude;
          console.log('response getting location', this.currentLocation);

          resolve(this.currentLocation);
        });

      } catch (err) {
        console.log("getLocation ERROR");
        reject(err);
      }
    });
  }
  validdateData() {
    if (this.form.status == 'VALID' && this.b64Image != undefined) {
      return true;
    }
    else {
      return false;
    }

  }
  async onClickSubmitBtn() {
    this.loading = await this.loadingCtrl.create({
      spinner: null,
      message: "<img  src='../../../assets/icon/loading.gif'>",
      // duration: 5000,
      translucent: true,
      cssClass: 'custom-loading',
    });
    this.loading.present();
    let timestamp = "2020-04-24 T08:45:50.621Z";
    const asiatime = new Date();
    asiatime.setHours(asiatime.getHours() + 7);
    timestamp = asiatime.toISOString();

    await this.getLocation();
    console.log("after getlocation");
    console.log(this.currentLocation);
    if (this.form.status == 'VALID') {
      let dataParam = {
        "currGeo": {
          "lat": this.currentLocation.lat,
          "lng": this.currentLocation.lng
        },
        "polDesc": this.form.value.polDesc,
        "b64Image": this.b64Image,
        "timestamp": timestamp,
        "polType": this.form.value.polType.trim()

      }
      console.log("VALID onClickSubmitBtn dataParam:");
      console.log(dataParam);
      try {
        let response = await this.polserviceService.ReportPolSource(dataParam);
        console.log(response);
        this.alert("Report Done");

      } catch (err) {
        this.alert(err.status);

      } finally {
        this.loading.dismiss();
      }
    }

  }
  async alert(msg) {
    let alertPopup;
    if (msg == -3) {
      alertPopup = await this.alertCtrl.create({
        message: '<ion-icon class="remindIcon iconRed" name="alert-outline"></ion-icon>Sorry, please try again.',
        buttons: [{
          text: 'OK',
          cssClass: 'alertBtn alertBtnFull',
          handler: data => {
            console.log('OK clicked');
          }
        }]
      });
    }
    else {
      alertPopup = await this.alertCtrl.create({
        message: '<ion-icon class="remindIcon iconSuccess" name="checkmark-outline"></ion-icon>' + msg,
        buttons: [{
          text: 'OK',
          cssClass: 'alertBtn alertBtnFull',
          handler: data => {
            console.log('OK clicked');
            this.clearData();
          }
        }]
      });

    }
    alertPopup.present();

  }

}
