import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReportTabPage } from './ReportTab.page';
import { ComponentsModule } from '../components/component.module';

import { ReportTabPageRoutingModule } from './ReportTab-routing.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild([{ path: '', component: ReportTabPage }]),
    ReportTabPageRoutingModule,
  ],
  declarations: [ReportTabPage]
})
export class ReportTabPageModule {}
