import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'countryMapTab',
        loadChildren: () => import('../countryMapTab/countryMapTab.module').then(m => m.countryMapTabPageModule)
      },
      {
        path: 'KnowledgeTab',
        loadChildren: () => import('../KnowledgeTab/KnowledgeTab.module').then(m => m.KnowledgeTabPageModule)
      },
      {
        path: 'yourLocationTab',
        loadChildren: () => import('../your-location-tab/your-location-tab.module').then( m => m.YourLocationTabPageModule)
      },
      {
        path: 'ReportTab',
        loadChildren: () => import('../ReportTab/ReportTab.module').then(m => m.ReportTabPageModule)
      },
      {
        path: 'myprofile',
        loadChildren: () => import('../myprofile/myprofile.module').then( m => m.MyprofilePageModule)
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/yourLocationTab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
