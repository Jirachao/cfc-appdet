import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {HereMapComponent} from './here-map/here-map.component';
import { HeaderContainerComponent } from './header-container/header-container.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [HereMapComponent,HeaderContainerComponent],
  exports: [HereMapComponent,HeaderContainerComponent]
})
export class ComponentsModule {}
