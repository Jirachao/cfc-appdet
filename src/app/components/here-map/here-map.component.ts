import { Component, OnInit, Input, NgZone } from '@angular/core';
import { GeolocationService } from '../../services/geolocation.service';
import { PolserviceService } from '../../services/polservice.service'
import { AlertController, LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { BehaviorSubject } from 'rxjs';
import { BluetoothService } from '../../services/bluetooth.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

declare var H: any;
@Component({
  selector: 'app-here-map',
  templateUrl: './here-map.component.html',
  styleUrls: ['./here-map.component.scss'],
})
export class HereMapComponent implements OnInit {

  constructor(private geolocation: GeolocationService, private zone: NgZone, public polserviceService: PolserviceService, public alertCtrl: AlertController, public loadingCtrl: LoadingController, private bluetooth: BluetoothService) {
  }
  @Input() namePage: string;
  countryPage: boolean = false;
  idMap: string = "";
  idChart: string = "";
  idBoxPol: string = "";
  map: any;
  platform: any;
  private hereMapApiKey: string = "W05qLuKpRghAelOnLl7kovI0QAJ99Znm_n2VPeWowXk";
  private AUTOCOMPLETION_URL = 'https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json';
  ajaxRequest = new XMLHttpRequest();
  query = '';
  isSearch = false;

  private firstTime = true;
  private chartMin = 0;
  private chartMax = 500.4;
  private TIME_LOOP: number = 60000;
  private TOTALDOTS: number = 24 * (this.TIME_LOOP / 1000);
  private FIRSTMEAN: number = (25 * this.TOTALDOTS) / this.TOTALDOTS;
  zoomValue: number = 2;
  getZoom: number = 18;
  currentLocation = { lat: 0, lng: 0 };
  timerVar: any;
  loading = this.loadingCtrl.create({
    spinner: null,
    message: "<img  src='../../../assets/icon/loading.gif'>",
    duration: 10000,
    translucent: true,
    cssClass: 'custom-loading',
  });
  time: BehaviorSubject<string> = new BehaviorSubject('00:00');
  timer: number;
  interval;
  hour = String('0' + Math.floor(0));
  polRes: any = {
    polType: "",
    polLev: 0,
    unit: ""
  };
  getPolRes: any;
  averageLevNew: number = 0;
  averageLevOld: number = 0;
  maxDistance: number = 0;
  countCallService: number = 1;
  remindColor: string = "Green";
  recommendMsg: string = "";
  CircleColor = {};
  bearsMarkers: any;
  snoozeState = false;
  ignoreState = false;
  snoozeTime = 0;
  minSnooze = 1;

  async ngOnInit() {
    this.firstTime = true;
    (await this.loading).present();
    await this.getLocation();
    let lat = this.currentLocation.lat;
    let lng = this.currentLocation.lng;

    if (this.namePage == "countryMapTab") {
      this.countryPage = true;
      this.zoomValue = 8;
      this.idMap = "mapcontainer";
      this.idChart = "chartdiv";
      this.idBoxPol = "getLevSpan";
      this.maxDistance = 1;
    }
    else if (this.namePage == "yourLocationTab") {
      this.countryPage = false;
      this.zoomValue = 18;
      this.idMap = "mapcontainer2";
      this.idChart = "chartdiv2";
      this.idBoxPol = "getLevSpan2";
      this.maxDistance = 0.00200;

      // var lessList = [10, 25, 26, 750, 25, 26, 10, 90, 30, 20, 10, 60];

      await this.updateCurrentPol(lat, lng);
      // await this.updateCurrentPol(lat, lng, lessList[0]);
      this.timerVar = Observable.interval(this.TIME_LOOP).subscribe(async x => {
        await this.getLocation();
        let lat2 = this.currentLocation.lat;
        let lng2 = this.currentLocation.lng;
        this.timerVar = x;
        console.log("loop updateCurrentPol " + this.timerVar + ":  lat2:" + lat2 + "   |lng2:" + lng2);
        this.countCallService += 1;
        await this.updateCurrentPol(lat2, lng2);
        // await this.updateCurrentPol(lat2, lng2, lessList[this.countCallService - 1]);
      });
    }

    this.zone.runOutsideAngular(async () => {
      this.createMap(lat, lng);
    });

  }
  searchLocationMAp(textBox, event) {
    console.log("textBox:", textBox);
    console.log("event:", event);

    if (this.query != textBox) {
      if (textBox.length >= 1) {
        this.isSearch=true;
        var params = '?' +
          'query=' + encodeURIComponent(textBox) + // The search text which is the basis of the query
          '&beginHighlight=' + encodeURIComponent('<mark>') + //  Mark the beginning of the match in a token.
          '&endHighlight=' + encodeURIComponent('</mark>') + //  Mark the end of the match in a token.
          '&maxresults=5' + // The upper limit the for number of suggestions to be included
          '&apikey=' + this.hereMapApiKey;
        this.ajaxRequest.open('GET', this.AUTOCOMPLETION_URL + params);
        this.ajaxRequest.send();
      }else{
        this.isSearch=false;
      }
    }
    else{
      this.isSearch=false;
    }
    this.query = textBox;
  }
  startTimer(duration: number) {
    clearInterval(this.interval);
    this.timer = duration * 60;
    this.updateTimeValue();
    this.interval = setInterval(() => {
      this.updateTimeValue();
    }, 1000);
  }
  async updateTimeValue() {
    let min: any = this.timer / 60;
    let sec: any = this.timer % 60;
    min = String('0' + Math.floor(min)).slice(-2);
    sec = String('0' + Math.floor(sec)).slice(-2);
    console.log("updateTimeValue        >>>   min:" + min + "sec" + sec);
    const safeTime = min + ":" + sec;
    if (!(min < 0 && sec < 0)) {
      this.time.next(safeTime);
    }
    if (this.timer <= 0) {
      if (!this.snoozeState && !this.ignoreState) {
        this.snoozeState = true;
        this.presentConfirm();
      }
      if (this.snoozeTime > 0) {
        if (this.snoozeTime < (this.minSnooze * 60)) {
          this.snoozeTime++;
        } else {
          this.snoozeState = false;
          this.snoozeTime = 0;
        }
      }

    } else {
      this.timer--;
    }
  }
  clearTime(message: string) {
    clearInterval(this.interval);
    this.time.next(message);
  }
  async presentConfirm() {
    let alert = await this.alertCtrl.create({
      message: '<ion-icon class="remindIcon iconRed" name="alert-outline"></ion-icon> Look like You stay too long in the unhealthy zone, it is recommended to leave the area as soon as possible.',
      buttons: [
        {
          text: 'Ignore',
          role: 'cancel',
          cssClass: 'alertBtn',
          handler: () => {
            console.log('Cancel clicked');
            this.ignoreState = true;
            this.snoozeTime = 0;
          }
        },
        {
          text: 'Snooze',
          cssClass: 'alertBtn',
          handler: () => {
            console.log('Buy clicked');
            this.snoozeTime = 1;
          }
        }
      ]
    });
    alert.present();
  }
  createGaugesChart(polLevel) {
    console.log("createGaugesChart >> polLevel:" + Number(polLevel));
    let dataPol = Number(polLevel);
    if (dataPol > this.chartMax) {
      console.log("dataPol" + dataPol);
      dataPol = this.chartMax;
      console.log("dataPol2" + dataPol);
    }
    var data = {
      score: dataPol,
      gradingData: [{
        titlePol: "Good",
        color: "#5cc93d",
        colorLabelFill: "#5cc93d",
        colorLabelStroke: "#5cc93d",
        lowScore: -1,
        highScore: 12.0
      },
      {
        titlePol: "Moderate",
        color: "#fefd53",
        colorLabelFill: "#ffd400",
        colorLabelStroke: "#ffff59",
        lowScore: 12.1,
        highScore: 35.4
      },
      {
        titlePol: "Sensitive",
        color: "#de8e37",
        colorLabelFill: "#de8e37",
        colorLabelStroke: "#de8e37",
        lowScore: 35.5,
        highScore: 55.4
      },
      {
        titlePol: "Unhealthy",
        color: "#ec3223",
        colorLabelFill: "#ec3223",
        colorLabelStroke: "#ec3223",
        lowScore: 55.5,
        highScore: 150.4
      },
      {
        titlePol: "Very Unhealthy",
        color: "#942048",
        colorLabelFill: "#942048",
        colorLabelStroke: "#942048",
        lowScore: 150.5,
        highScore: 250.4
      },
      {
        titlePol: "Hazardous",
        color: "#751425",
        colorLabelFill: "#751425",
        colorLabelStroke: "#751425",
        lowScore: 250.5,
        highScore: 500.4
      }
      ]
    };

    // create chart
    var chart = am4core.create(this.idChart, am4charts.GaugeChart);
    chart.hiddenState.properties.opacity = 0;
    chart.fontSize = "0.5em"; //label scal
    chart.innerRadius = am4core.percent(85);
    chart.resizable = true;
    chart.logo.disabled = true;
    chart.responsive.enabled = true;

    var axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());
    axis.min = this.chartMin;
    axis.max = this.chartMax;
    axis.strictMinMax = true;
    axis.renderer.radius = am4core.percent(80);
    axis.renderer.inside = true;
    axis.renderer.line.strokeOpacity = 0.1;
    axis.renderer.ticks.template.disabled = false;
    axis.renderer.ticks.template.strokeOpacity = 1;
    axis.renderer.ticks.template.length = 3;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.radius = am4core.percent(25);
    axis.renderer.labels.template.fontSize = "1vw";
    axis.renderer.labels.template.fill = am4core.color("#e0fbfc");;

    var axis2 = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());
    axis2.min = this.chartMin;
    axis2.max = this.chartMax;
    axis2.strictMinMax = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.ticks.template.disabled = true;
    axis2.renderer.grid.template.disabled = false;
    axis2.renderer.grid.template.opacity = 0.5;
    axis2.renderer.labels.template.bent = true;

    for (let grading of data.gradingData) {
      var range = axis2.axisRanges.create();
      range.axisFill.fill = am4core.color(grading.color);
      range.axisFill.fillOpacity = 0.8;
      range.axisFill.zIndex = -1;
      range.value = grading.lowScore > this.chartMin ? grading.lowScore : this.chartMin;
      range.endValue = grading.highScore < this.chartMax ? grading.highScore : this.chartMax;
      range.grid.strokeOpacity = 0;
      range.label.text = "";
      range.label.fill = am4core.color("#e0fbfc");
      range.label.location = 0.5;
      range.label.inside = true;

    }

    var matchingGrade = lookUpGrade(data.score, data.gradingData);

    var label = chart.radarContainer.createChild(am4core.Label);
    label.isMeasured = false;
    label.fontSize = "3.5vw";
    label.x = am4core.percent(50);
    label.paddingBottom = 13;
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";
    label.text = polLevel;//data.score.toFixed(1);
    label.fill = am4core.color(matchingGrade.color);

    var label2 = chart.radarContainer.createChild(am4core.Label);
    label2.isMeasured = false;
    label2.fontSize = "3vw";
    label2.horizontalCenter = "middle";
    label2.verticalCenter = "bottom";
    // label2.text = matchingGrade.titlePol;
    label2.fill = am4core.color(matchingGrade.color);

    var hand = chart.hands.push(new am4charts.ClockHand());
    hand.axis = axis2;
    hand.innerRadius = am4core.percent(50);
    hand.radius = am4core.percent(90);
    hand.startWidth = 3;
    hand.pin.disabled = true;
    hand.value = data.score;
    hand.fill = am4core.color("#6094e2");
    hand.stroke = am4core.color("#6094e2");

    hand.events.on("positionchanged", function () {
      label.text = axis2.positionToValue(hand.currentPosition).toFixed(1);
      var matchingGrade = lookUpGrade(axis.positionToValue(hand.currentPosition), data.gradingData);
      label2.text = matchingGrade.titlePol;
      label2.fill = am4core.color(matchingGrade.colorLabelFill);
      label2.stroke = am4core.color(matchingGrade.colorLabelStroke);
      label.fill = am4core.color(matchingGrade.colorLabelFill);
      label.stroke = am4core.color(matchingGrade.colorLabelStroke);
      label.text = polLevel;
    })

    setInterval(function () {
      hand.showValue(data.score, 1000, am4core.ease.cubicOut);
    }, 2000);

    function lookUpGrade(lookupScore, grades) {
      for (var i = 0; i < grades.length; i++) {
        if (
          grades[i].lowScore < lookupScore &&
          grades[i].highScore >= lookupScore
        ) {
          return grades[i];
        }
      }
      return null;
    }

  }
  getLocation() {
    return new Promise(async (resolve, reject) => {
      try {
        console.log("getLocation");
        this.geolocation.getLatLon().then((location: any) => {
          this.currentLocation.lat = location.latitude;
          this.currentLocation.lng = location.longitude;
          console.log('response getting location', this.currentLocation);

          resolve(this.currentLocation);
        });

      } catch (err) {
        console.log("getLocation ERROR", err);
        reject(err);
      }
    });
  }
  createColorChart(polListLev) {
    if (polListLev >= 0 && polListLev <= 12.0) {
      this.remindColor = "Green";
      this.recommendMsg = "Enjoy";
      this.CircleColor = {
        strokeColor: 'rgba(63, 195, 128, 0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(63, 195, 128, 0.3)' // Color of the circle
      }
    } else if (polListLev >= 12.1 && polListLev <= 35.4) {
      this.remindColor = "Yellow";
      this.recommendMsg = "Be Careful";
      this.CircleColor = {
        strokeColor: 'rgba(240, 255, 0, 0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(240, 255, 0, 0.3)' // Color of the circle
      }
    } else if (polListLev >= 35.5 && polListLev <= 55.4) {
      this.remindColor = "Red";
      this.recommendMsg = "Wear Mask";
      this.CircleColor = {
        strokeColor: 'rgba(244, 179, 80, 0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(244, 179, 80, 0.3)' // Color of the circle
      }
    } else if (polListLev >= 55.5 && polListLev <= 150.4) {
      this.remindColor = "Red";
      this.recommendMsg = "Wear Mask";
      this.CircleColor = {
        strokeColor: 'rgba(207, 0, 15, 0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(207, 0, 15, 0.2)' // Color of the circle
      }
    } else if (polListLev >= 150.5 && polListLev <= 250.4) {
      this.remindColor = "RedDark";
      this.recommendMsg = "Leave the area";
      this.CircleColor = {
        strokeColor: 'rgba(102, 51, 153,0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(102, 51, 153,0.2)' // Color of the circle
      }
    } else if (polListLev >= 250.5 && polListLev <= 500.4) {
      this.remindColor = "RedDark";
      this.recommendMsg = "Leave the area";
      this.CircleColor = {
        strokeColor: 'rgba(150, 40, 27,0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(150, 40, 27,0.3)' // Color of the circle
      }
    } else if (polListLev > 500.4) {
      this.remindColor = "RedDark";
      this.recommendMsg = "Leave the area";
      this.CircleColor = {
        strokeColor: 'rgba(150, 40, 27,0.5)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(150, 40, 27,0.3)' // Color of the circle
      }
    } else {
      console.log("else color> polLev:", polListLev);
      this.CircleColor = {
        strokeColor: 'rgba(150, 40, 27, 0)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(150, 40, 27, 0)' // Color of the circle
      }
    }
    console.log("@createColorChart    polListLev:" + polListLev + "|| remindColor:" + this.remindColor + " ||recommendMsg" + this.recommendMsg);

  }
  async createMap(latitude, longitude) {
    (await this.loading).present();
    var that = this;
    setTimeout(function () {
      that.platform = new H.service.Platform({
        'apikey': that.hereMapApiKey
      });
      var defaultLayers = that.platform.createDefaultLayers();

      that.map = new H.Map(document.getElementById(that.idMap),
        defaultLayers.vector.normal.map, {
        center: { lat: latitude, lng: longitude },
        zoom: that.zoomValue,
        pixelRatio: window.devicePixelRatio || 1
      });

      // add a resize listener to make sure that the map occupies the whole container
      window.addEventListener('resize', () => that.map.getViewPort().resize());

      //Step 3: make the map interactive
      // MapEvents enables the event system
      // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
      var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(that.map));

      // Create the default UI components
      var ui = H.ui.UI.createDefault(that.map, defaultLayers);

      that.map.addEventListener('mapviewchangeend', async () => { // prints correct zoom level? 

        var center = String(that.map.getCenter());
        var res = center.split(" ");
        let lat2 = res[2].substring(0, res[2].length - 1);
        let lng2 = res[1].substring(1, res[1].length);
        console.log("zoom center :lat2:" + lat2 + "   |lng2:" + lng2 + "   |getZoom:" + that.map.getZoom());

        /* Calculate for delay call Operation
        await that.getLocation();
        const latLocation = that.currentLocation.lat;
        const lngLocation = that.currentLocation.lng;
        var cal1 = Math.pow(Number(lat2) - latLocation, 2);
         var cal2 = Math.pow(Number(lng2) - lngLocation, 2);
         console.log("cal1:" + cal1 + "   ||cal2:" + cal2 + "    ||latLocation:" + latLocation + "   ||lngLocation:" + lngLocation);
         var sqrtCal = Number(Math.sqrt(cal1 + cal2).toFixed(5));
         console.log("sqrtCal:" + sqrtCal);
         if (sqrtCal > that.maxDistance) {*/
        if (that.namePage == "countryMapTab") {
          var x = document.getElementById('geoListShow');
          x.style.display = "none";
        }
        if(that.firstTime){
          console.log("firstTime");
          await that.GetPolData(latitude, longitude, that.getZoom);
          that.firstTime=false;
        }else{
          console.log("!firstTime");
          var getZoom2 = that.map.getZoom();
          await that.GetPolData(lat2, lng2, getZoom2);
        }
        that.createCirclePol(that.map);
        // }

      });
      that.getSerach(that.map);

    }, 2000);
  }
  getSerach(map) {
    console.log("getSerach");
    console.log("query", this.query);
    //Search
    var groupSearch = new H.map.Group();
    var geocoder = this.platform.getGeocodingService();
    var locationOld=0;
    var that=this;
    // Attach the event listeners to the XMLHttpRequest object
    this.ajaxRequest.addEventListener("load", function () {
      groupSearch.removeAll();
      //addSuggestionsToPanel
      var suggestions = JSON.stringify(this.response, null, ' ');
      // addSuggestionsToMap;
      var onGeocodeSuccess = function (result) {
        var marker;
        var locations = result.Response.View[0].Result;
      
        // Add a marker for each location found
        for (var i = 0; i < locations.length; i++) {
          marker = new H.map.Marker({
            lat: locations[i].Location.DisplayPosition.Latitude,
            lng: locations[i].Location.DisplayPosition.Longitude
          });
          // marker.setData(locations[i].Location.Address.Label);
          groupSearch.addObject(marker);
        }

        map.addObject(groupSearch);

        map.getViewModel().setLookAtData({
            bounds: groupSearch.getBoundingBox()
        });
        if (groupSearch.getObjects().length < 2) {
            map.setZoom(15);
        }
        console.log("locationOld:"+locationOld+"           groupSearch:"+groupSearch.getObjects().length+ "     isSearch:"+that.isSearch);
        if(groupSearch.getObjects().length != locationOld){
          that.isSearch = true;
          console.log("IF getSerach:  isSearch"+ that.isSearch);
        }else{
          that.isSearch = false;
          console.log("elseIF getSerach:  isSearch"+ that.isSearch);
        }
        locationOld=groupSearch.getObjects().length;
      },
        onGeocodeError = function (error) {
          alert('Ooops!');
        },
        geocodeByLocationId = function (locationId) {
          const geocodingParameters = {
            locationId: locationId
          };
          geocoder.geocode(
            geocodingParameters,
            onGeocodeSuccess,
            onGeocodeError
          );
        }
      this.response.suggestions.forEach(function (item, index, array) {
        geocodeByLocationId(item.locationId);
      });
    });
    this.ajaxRequest.addEventListener("error", function () {
      alert('Ooops!');
    });
    this.ajaxRequest.responseType = "json";

    map.addObject(groupSearch);
  }
  async createCirclePol(map) {
    console.log("createCirclePol",map);
    if (map.getObjects() != undefined) {
      if(this.query==""){
        console.log("this.query=empty");
        map.removeObjects(map.getObjects());
      }else{
        console.log("this.query!=empty");
        if(this.isSearch ==false){
          // map.removeObjects(map.getObjects());
        }
        console.log("createCirclePol :  isSearch:",this.isSearch);
      }
      
      //Pin Your location
      if (this.namePage == "yourLocationTab") {
        await this.getLocation();
        //Mark your location
        var yourLocation = new H.map.Icon("../../../assets/icon/placeholder.svg", { size: { w: 50, h: 50 } });
        var yourLocationMarker = new H.map.Marker({
          lat: this.currentLocation.lat,
          lng: this.currentLocation.lng
        }, { icon: yourLocation });
        map.addObject(yourLocationMarker);
        this.createColorChart(this.polRes.polLev);

        console.log("1 circle CircleColor::" + this.CircleColor);
        var circle = new H.map.Circle({ lat: this.currentLocation.lat, lng: this.currentLocation.lng }, 100, {
          style: this.CircleColor
        });
        map.addObject(circle);
      }
      // Pol list pm2.5
      var that = this;
      for (var i = 0; i < that.getPolRes.length; i++) {
        var colorPolListLev = '';
        var polListLev = that.getPolRes[i].polList[0].pol.lev;
        if (polListLev >= 0 && polListLev <= 12.0) {
          colorPolListLev = '#5bb043';
        } else if (polListLev >= 12.1 && polListLev <= 35.4) {
          colorPolListLev = '#dbd954';
        } else if (polListLev >= 35.5 && polListLev <= 55.4) {
          colorPolListLev = '#c2803d';
        } else if (polListLev >= 55.5 && polListLev <= 150.4) {
          colorPolListLev = '#ce3a2e';
        } else if (polListLev >= 150.5 && polListLev <= 250.4) {
          colorPolListLev = '#872b49';
        } else if (polListLev >= 250.5 && polListLev <= 500.4) {
          colorPolListLev = '#6e222e';
        } else if (polListLev > 500.4) {
          colorPolListLev = '#6e222e';
        } else {
          console.log("else color> polLev:", polListLev);
        }
        var innerElement = document.createElement('div');
        innerElement.setAttribute("id", polListLev);
        innerElement.style.backgroundColor = colorPolListLev;
        innerElement.style.border = '1px solid #5a5858';
        innerElement.style.borderRadius = '5vw';
        innerElement.style.lineHeight = '2vh';
        innerElement.style.textAlign = 'center';
        innerElement.style.paddingTop = '0.5em';//0.8vw
        innerElement.style.fontSize = '1em';
        innerElement.style.width = '2em';
        innerElement.style.height = '2em';

        // Add text to the DOM element 
        innerElement.innerHTML = polListLev;
        function showDetailList(evt) {
          if (that.namePage == "countryMapTab") {
            var x = document.getElementById('geoListShow');
            if (x.style.display === "none") {
              x.style.display = "block";
            } else {
              x.style.display = "none";
            }

            that.createColorChart(evt.path[0].id);
            var getLevSpan = document.getElementById(that.idBoxPol);
            getLevSpan.innerHTML = evt.path[0].id + " µg/m" + "3".sup();
            that.createGaugesChart(evt.path[0].id);
          }

        };

        var domIcon = new H.map.DomIcon(innerElement, {
          // the function is called every time marker enters the viewport onAttach ||touchstart
          onAttach: function (clonedElement, domIcon, domMarker) {
            clonedElement.addEventListener('touchstart', showDetailList);
          }, onDetach: function (clonedElement, domIcon, domMarker) {
            clonedElement.removeEventListener('touchstart', showDetailList);
          }
        });

        const latPol = that.getPolRes[i].lat;
        const lonPol = that.getPolRes[i].lon;
        that.bearsMarkers = new H.map.DomMarker({ lat: latPol, lng: lonPol }, {
          icon: domIcon
        });
        map.addObject(that.bearsMarkers);
      }
    }


  }
  calAverageLevel(levelNew) {
    console.log("calAverageLevel:" + levelNew);

    if (this.countCallService < 2) {
      this.averageLevNew = Number(((this.FIRSTMEAN * (this.TOTALDOTS - 1) + levelNew) / this.TOTALDOTS).toFixed(5));
      this.averageLevOld = this.FIRSTMEAN;
    }
    else {
      this.averageLevNew = Number(((this.averageLevOld * (this.TOTALDOTS - 1) + levelNew) / this.TOTALDOTS).toFixed(5));
    }

    console.log("averageLevNew in :" + this.averageLevNew);
    var m = Number((this.averageLevNew - this.averageLevOld).toFixed(5));
    console.log("m in :" + m);
    if (m > 0) {
      if (this.averageLevNew <= 25) {
        console.log("Careful <=25 [case1]");
        var x = Number((((25 - this.averageLevNew) / m) + 1).toFixed(5));
        var timeOn = Number(((24 * x) / this.TOTALDOTS).toFixed(5));
        console.log("timeOn in :" + timeOn);
        this.startTimer(timeOn);
      } else {
        console.log("Escape >25 [case3]");
        this.clearTime("Unhealthy");
      }
    } else {
      if (this.averageLevNew <= 25) {
        console.log("inhabit <=25 [case2]");
        this.ignoreState = false;
        this.clearTime("Safe");
      } else {
        console.log("inhabit >25 [case4]");
        var x = Number((((25 - this.averageLevNew) / m) + 1).toFixed(5));
        var timeOn = Number(((24 * x) / this.TOTALDOTS).toFixed(5));
        console.log("timeOn in :" + timeOn);
        if (timeOn > 24) {
          this.clearTime("Unhealthy");
        } else {
          this.ignoreState = false;
          this.clearTime("Safe");
        }
      }
    }
    this.averageLevOld = this.averageLevNew;
    console.log("averageLevOld in :" + this.averageLevOld);

  }
  async updateCurrentPol(latitude, longitude) {
    let res: any;
    let polLevSensor;
    console.log("listToggle");
    console.log(this.bluetooth.listToggle);
    await this.bluetooth.getDustData().then((dust) => {
      polLevSensor = dust;

    }, (err) => {
    });
    let dataParam = {
      "currGeo": {
        "lat": latitude,
        "lon": longitude
      }
    }
    /*res = {
      "polList": [
        {
            "pol": [
              {
                "polTyp": "PM2.5",
                "polLev": ll,
                "unit": " µg/m" + "3".sup()
              }
            ]
        }
      ]
    };*/
    try {
      let response: any;
      response = await this.polserviceService.UpdateCurrentPol(dataParam);
      res = JSON.parse(response);
      console.log("updateCurrentPol(ts): respose");
      console.log(res);
      this.polRes.polType = res.polList[0].pol[0].polTyp;
      this.polRes.polLev = res.polList[0].pol[0].polLev;
      this.polRes.unit = res.polList[0].pol[0].unit;

      document.getElementById("geoListShow2").classList.remove("disabledDisplay");
      document.getElementById("geoListShow2").classList.add("showDisplay");

    } catch (err) {
      res = {
        "polList": [
          {
            "pol": [
              {
                "polTyp": "PM2.5",
                "polLev": "errorLeve",
                "unit": " µg/m" + "3".sup()
              }
            ]
          }
        ]
      };
      this.polRes.polLev = res.polList[0].pol[0].polLev;
      document.getElementById("geoListShow2").classList.add("disabledDisplay");
    }
    // this.polRes = res;
    this.polRes.polType = res.polList[0].pol[0].polTyp;
    this.polRes.unit = res.polList[0].pol[0].unit;

    var getLevSpan = document.getElementById(this.idBoxPol);
    if (this.polRes.polLev >= 0) {
      console.log("Continute >>> ", this.polRes.polLev);
      if (!this.bluetooth.listToggle) {
        getLevSpan.innerHTML = " No sensor";
        this.polRes.polLev = res.polList[0].pol[0].polLev;
      }
      else {
        console.log("polLevSensor" + polLevSensor)
        getLevSpan.innerHTML = polLevSensor + " µg/m" + "3".sup();
        this.polRes.polLev = polLevSensor;
      }
      this.createCirclePol(this.map);
      this.calAverageLevel(this.polRes.polLev);
      this.createGaugesChart(this.polRes.polLev);
      this.createColorChart(this.polRes.polLev);
    }

    return res

  }
  async GetPolData(latitude, longitude, getZoom) {
    let res: any;
    let response: any;
    let dataParam = {
      "currGeo": {
        "lat": latitude,
        "lng": longitude
      },
      "zoomFactor": getZoom
    }

    try {
      (await this.loading).present();
      response = await this.polserviceService.GetPolData(dataParam);
      res = JSON.parse(response);
      console.log("GetPolData(ts): res.geoList");
      console.log(res.geoList);
      this.getPolRes = res.geoList;
      return res.geoList;

    } catch (err) {
      res = {
        "geoList": []
      }

      this.getPolRes = res;
      return res;
    }


  }

}
