import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

declare let Paho: any;
var _client: any;
var _deviceId: any;
var _dust = '0';
var _lat = '0';
var _lon = '0';

@Injectable({
  providedIn: 'root'
})

export class WatsoniotService {

  constructor(private platform: Platform) { }

  sendWatsoniotData(clientId,deviceId,dust,lat,lon){
    //return new Promise((resolve, reject) => {
      _client =  clientId;
      _deviceId = deviceId;
      _dust = dust;
      _lat = lat;
      _lon = lon;
      this.connectWatsoniot();
      //resolve(true);
    //});
  }

  connectWatsoniot() {
    this.platform.ready().then(() => {
      //this.statusBar.styleDefault();
      //this.splashScreen.hide();
      //_client = new Paho.MQTT.Client("gesjst.messaging.internetofthings.ibmcloud.com", 8883, "d:gesjst:Android:SM-950F");
      _client = new Paho.MQTT.Client("svpasu.messaging.internetofthings.ibmcloud.com", 8883, _client);
      _client.onConnectionLost = this.onConnectionLost;
      _client.onMessageArrived = this.onMessageArrived;
      var options = {
        useSSL: true,
        userName: "use-token-auth",
        password: "myauthtoken",
        onSuccess:this.onConnect,
        onFailure:this.doFail,
        timeout:30
      }

      _client.connect(options);
    });
  }

  onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    console.log("onConnect");
    var str = {
      "deviceType": "iotsensor_device",
      "deviceId": "nodered-device-007",
      "timestamp": "2020-04-24 T08:45:50.621Z",
      "geoLoc": {
        "lat": "1234",
        "lon": "23423"
      },
      "polData": [{
        "sensorID": "12QS123S",
        "data": [{
          "type": "PM2.5",
          "lev": "1.2312",
          "unit": "µg/m3"
        }],
        "sensorType": "mobile"
      }]
    };
    const asiatime = new Date();
    asiatime.setHours( asiatime.getHours() + 7 );
    str.timestamp = asiatime.toISOString();
    str.deviceId = _deviceId;
    str.polData[0].data[0].lev = _dust;
    str.geoLoc.lat = _lat;
    str.geoLoc.lon = _lon;
    console.log(JSON.stringify(str));
    var message = new Paho.MQTT.Message(JSON.stringify(str));
      message.destinationName = "iot-2/evt/sensorData/fmt/json";
      _client.send(message);
  }

  onMessageArrived(message) {
    console.log("onMessageArrived:"+message.payloadString);
  }
  
  doFail(e){
    console.log(e);
  }

  // called when the client loses its connection
  onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
      console.log("onConnectionLost:"+responseObject.errorMessage);
    }
  }

}
