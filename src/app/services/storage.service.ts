import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }

  saveStorage(key,obj){
    return new Promise((resolve, reject) => {
      this.storage.set(key, obj).then((val) => {
        resolve(true);
      }).catch(err => {
        console.log(err);
        reject(false);
      });
  });
  }

  loadStorage(key){
    return new Promise((resolve, reject) => {
      this.storage.get(key).then((val) => {
        console.log("loadStorage>>"+val);
        resolve(val);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
  });
  }

  clearStorage(){
    return new Promise((resolve, reject) => {
      this.storage.clear().then((val) => {
        resolve(true);
      }).catch(err => {
        console.log(err);
        reject(false);
      });
  });
  }

  removeStorage(key){
    return new Promise((resolve, reject) => {
      this.storage.remove(key).then((val) => {
        resolve(true);
      }).catch(err => {
        console.log(err);
        reject(false);
      });
  });
  }

}
