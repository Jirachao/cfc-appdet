import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Device } from '@ionic-native/device/ngx';

@Injectable({
  providedIn: 'root'
})
export class PolserviceService {

  private headers: any = {
    'Content-Type': 'application/json'
  };

  constructor(private device: Device, private http: HTTP) { }

  ReportPolSource(dataParam) {
    let param = {
      'currGeo': {
        'lat': dataParam.currGeo.lat,
        'lon': dataParam.currGeo.lng
      },
      'polDesc': dataParam.polDesc,
      'b64Image': dataParam.b64Image,
      'timestamp': dataParam.timestamp,
      'uid': "facebookID",
      'polType': dataParam.polType
    }
    console.log("ReportPolSource >> param:", param);
    const SERVICE_URL = 'https://node-red-lclwm.au-syd.mybluemix.net/app-det/polservice/report';
    return new Promise((resolve, reject) => {
      this.http.post(SERVICE_URL, param, this.headers)
        .then(data => {
          console.log("ReportPolSource <response>" + data);
          resolve(data.data);
        })
        .catch(error => {
          console.log("ReportPolSource <response Error>");
          console.log(error);
          reject(error);
        });
    });
  }

  UpdateCurrentPol(dataParam) {
    const SERVICE_URL = 'https://node-red-lclwm.au-syd.mybluemix.net/app-det/polservice/getcurrent';

    dataParam={
      "currGeo": {
        "lat": dataParam.currGeo.lat,
        "lon": dataParam.currGeo.lon
      },
      "deviceId": this.device.uuid
    };
    console.log("UpdateCurrentPol >> dataParam:", dataParam);
    return new Promise((resolve, reject) => {
      this.http.setDataSerializer('json');
      this.http.post(SERVICE_URL, dataParam, this.headers)
        .then(data => {
          console.log("updateCurrentPol 1 <response>" + JSON.stringify(data.data));
          resolve(data.data);
        })
        .catch(error => {
          console.log("updateCurrentPol <response Error>" + JSON.stringify(error));
          reject(error);
        });
    });

  }
  
  GetPolData(dataParam) {
    let param = {
      "currGeo": {
        "lat": dataParam.currGeo.lat,
        "lon": dataParam.currGeo.lng
      },
      "deviceId": this.device.uuid,
      "zoomFactor": dataParam.zoomFactor
    };
    console.log("GetPolData >> param:", param);
    const SERVICE_URL = 'https://node-red-lclwm.au-syd.mybluemix.net/app-det/polservice/get';

    return new Promise((resolve, reject) => {
      this.http.setDataSerializer('json');
      this.http.post(SERVICE_URL, param, this.headers)
        .then(data => {
          console.log("GetPolData 1 <response>" + JSON.stringify(data.data));
          resolve(data.data);
        })
        .catch(error => {
          console.log("GetPolData <response Error>" + JSON.stringify(error));
          reject(error);
        });
    });
  }
}