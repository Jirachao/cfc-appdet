import { Injectable } from '@angular/core';
//import { HttpClient, HttpHeaders, Headers, RequestOptions } from '@angular/common/http';
//import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';


@Injectable({
  providedIn: 'root'
})
export class RestapiService {

  registerUrl = 'https://node-red-lclwm.au-syd.mybluemix.net/app-det/deviceservice/regis';
  testUrl = 'https://node-red-lclwm.au-syd.mybluemix.net/app-det/polservice/getcurrent';
  userprofileUrl = 'https://node-red-lclwm.au-syd.mybluemix.net/app-det/userprofile/get';

  constructor(private http: HTTP) { 
    console.log('Hello RestServiceProvider Provider');
  }

  
  registerDevice(data) {
    return new Promise((resolve, reject) => {
      
        let headers = {
            'Content-Type': 'application/json'
        };

        this.http.post(this.registerUrl, data, headers)
        .then(res => {
          console.log("<response>" + res);
          resolve(res);
        })
        .catch(err => {
          console.log("<response Error>" + err);
          reject(err);
        });
    });
  }

  getUserProfile(data) {
    return new Promise((resolve, reject) => {
      
        let headers = {
            'Content-Type': 'application/json'
        };

        this.http.post(this.userprofileUrl, data, headers)
        .then(res => {
          console.log("<response>" + res);
          resolve(res);
        })
        .catch(err => {
          console.log("<response Error>" + err);
          reject(err);
        });
    });
  }

  /*registerDevice(data) {
    return new Promise((resolve, reject) => {
      
        let headers = {
            'Content-Type': 'application/json'
        };

        this.http.post(this.registerUrl, data, {headers: new HttpHeaders().set('Content-Type', 'application/json')}
          
        ).subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }*/
}
