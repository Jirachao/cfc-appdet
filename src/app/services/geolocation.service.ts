import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  constructor(private geolocation: Geolocation) { }

  getLatLon(){
    return new Promise((resolve, reject) => {
      this.geolocation.getCurrentPosition().then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        //console.log(resp.coords.latitude);
        //console.log(resp.coords.longitude);
        let location = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
        resolve(location)
       }).catch((error) => {
         console.log('Error getting location', error);
         reject(error);
       });
    });
  }
}
