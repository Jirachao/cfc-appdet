import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Injectable({
  providedIn: 'root'
})
export class PushnotificationService {

  constructor(private firebaseX: FirebaseX, private localNotifications: LocalNotifications,private toastCtrl: ToastController) { }

  initializeFirebaseAndroid() {
    return new Promise((resolve, reject) => {
      this.firebaseX.getToken().then(token => {
        //this.firebaseX.onTokenRefresh().subscribe(token => {})
        this.subscribeToPushNotifications();
        resolve(token)
      }).catch(err => {
        console.log(err);
        reject(err);
      });
      
    });
  }

  subscribeToPushNotifications() {
    this.firebaseX.onMessageReceived().subscribe((response) => {
      if(response.tap){
        console.log("background mode =>"+response.body);
        this.showToast(response.body);
      }else{
        //received while app in foreground (show a toast)
        console.log("forground mode =>"+response.body);
        this.showToast(response.body);
       
      }
    });
  }

  async showToast(msj) {
    const toast = await this.toastCtrl.create({
      header: 'AP-DET',
      message: msj,
      position: 'top',
      cssClass: "my-toast",
      duration: 3000
    });
    toast.present();

  }
}
