import { TestBed } from '@angular/core/testing';

import { WatsoniotService } from './watsoniot.service';

describe('WatsoniotService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WatsoniotService = TestBed.get(WatsoniotService);
    expect(service).toBeTruthy();
  });
});
