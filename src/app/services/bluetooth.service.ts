import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';

@Injectable({
  providedIn: 'root'
})
export class BluetoothService {
  pairedList: pairedlist;
  public listToggle: boolean = false;
  constructor(private bluetoothSerial: BluetoothSerial, private toastCtrl: ToastController) { }

  checkBluetoothEnabled() {
    
    return new Promise((resolve, reject) => {
      this.bluetoothSerial.isEnabled().then(success => {
        this.listToggle = true;
        resolve(true);
      }, error => {
        this.listToggle = false;
        reject(false);
      });
  });
  }

  getDustData() {
    return new Promise((resolve, reject) => {
      this.bluetoothSerial.available().then((number: any) => {
        this.bluetoothSerial.write('S').then(success => {
          console.log(success);
          this.bluetoothSerial.read().then((data: any) => {
            console.log(JSON.stringify(data));
            let dust = '0';
            if (data==""||data==undefined){
              dust = '0';
            }else{
              dust = data;
            }
            resolve(dust);
            //this.showToast(JSON.stringify(data));
          });
        
        }, error => {
          //this.showError(error)
          reject('0');
        });
        
  
      }, error => {
        this.showError("Please Enable Bluetooth")
        this.listToggle = false;
      });
  });
  }
  
  listPairedDevices() {
    return new Promise((resolve, reject) => {
        this.bluetoothSerial.list().then(success => {
          this.pairedList = success;
          this.listToggle = true;
          resolve(this.pairedList);
        }, error => {
          //this.showError("Please Enable Bluetooth")
          this.listToggle = false;
          reject(false);
        });
    });
  }

  connect(data) {
    // Attempt to connect device with specified address, call app.deviceConnected if success
    return new Promise((resolve, reject) => {
        this.bluetoothSerial.connect(data.address).subscribe(success => {
          this.deviceConnected();
          //this.showToast("Successfully Connected");
          this.listToggle = true;
          resolve(true);
        }, error => {
          //this.showToast("Unsuccessfully Connected");
          this.listToggle = false;
          reject(false);
        });
    });
  }

  deviceConnected() {
    // Subscribe to data receiving as soon as the delimiter is read
    return new Promise((resolve, reject) => {
        this.bluetoothSerial.subscribe('\n').subscribe(success => {
          resolve(true);
        }, error => {
          //this.showError(error);
          reject(false);
        });
    });
  }

  showError(error) {
    alert(JSON.stringify(error));
  }

  async showToast(msj) {
    const toast = await this.toastCtrl.create({
      message: msj,
      duration: 1000
    });
    toast.present();

  }
}

interface pairedlist {
  "class": number,
  "id": string,
  "address": string,
  "name": string
}