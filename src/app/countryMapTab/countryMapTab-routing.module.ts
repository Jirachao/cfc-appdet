import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { countryMapTabPage } from './countryMapTab.page';

const routes: Routes = [
  {
    path: '',
    component: countryMapTabPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class countryMapTabPageRoutingModule {}
