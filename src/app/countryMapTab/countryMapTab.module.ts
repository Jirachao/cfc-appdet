import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { countryMapTabPage } from './countryMapTab.page';
import { ComponentsModule } from '../components/component.module';
import { countryMapTabPageRoutingModule } from './countryMapTab-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule,
    countryMapTabPageRoutingModule
  ],
  declarations: [countryMapTabPage]
})
export class countryMapTabPageModule {}
