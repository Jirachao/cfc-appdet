import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyprofilePageRoutingModule } from './myprofile-routing.module';
import { ComponentsModule } from '../components/component.module';

import { MyprofilePage } from './myprofile.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    MyprofilePageRoutingModule
  ],
  declarations: [MyprofilePage]
})
export class MyprofilePageModule {}
