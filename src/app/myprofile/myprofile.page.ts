import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { DevicelistPage } from '../devicelist/devicelist.page';
import { StorageService } from '../services/storage.service';
import { BluetoothService } from '../services/bluetooth.service';
import { WatsoniotService } from '../services/watsoniot.service';
import { GeolocationService } from '../services/geolocation.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { interval, Subscription } from 'rxjs';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { RestapiService } from '../services/restapi.service';
import { DomSanitizer } from '@angular/platform-browser';

const source = interval(60000);


@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit {
  public username: string;
  public password: string;
  type: string;
  constructor(public modalController: ModalController,public storage: StorageService, private toastCtrl: ToastController, private bluetooth: BluetoothService,public watson: WatsoniotService,private geoloation: GeolocationService, public backgroundMode : BackgroundMode, private localNotifications: LocalNotifications, public restProvider: RestapiService,private sanitizer: DomSanitizer) { }
  baseUrl = '../../assets/img/';
  imageUrl = '';
  sensorName = '';
  clientId = '';
  deviceId = '';
  address = '';
  listToggle: boolean = false;
  connectedDevice: any;
  isDisconnect: boolean = true;
  btn_txt = 'Pair';
  subscription: Subscription;
  timerVar;
  timerVal; 
  dustVal = '';
  alertDust=0;
  minLev=35.5;
  userReg : any = {};
  isSignIn = false;
  email = '';
  displayname = '';
  photo = '';
  async presentModal() {
    let modal = await this.modalController.create({
      component: DevicelistPage
    });
    await modal.present();

    const {data} = await modal.onWillDismiss();
    if (data.dismissed){
      this.isDisconnect = false;
      this.btn_txt = 'UnPair';
      this.showSensor();
      this.startTimmer();
    }
  }

  ngOnInit() {
    this.type = 'myprofile';
  }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }

  ionViewWillEnter(){
    this.showSensor();
    this.showUserProfile();
  }

  showSensor(){
    this.imageUrl = '';
    this.sensorName = '';
    this.clientId = '';
    this.deviceId = '';
    this.address = '';
    this.storage.loadStorage('sensor').then((result) => {
      let res:any;
      res = result;
      if (result!=null){
        this.connectedDevice = result;
        console.log("sensor>>"+result);
        this.imageUrl = this.baseUrl+res.name+'.jpg';
        this.sensorName = res.name;
        this.clientId = res.clientId;
        this.address = res.address;
        this.deviceId = res.deviceId;
        this.bluetooth.checkBluetoothEnabled().then((result:boolean) => {
          if (result){
            this.listToggle = this.bluetooth.listToggle;
            
          }
        }, (err) => {
          console.log(err);
          this.listToggle = this.bluetooth.listToggle;
          this.showToast("Please Enable Bluetooth")
        });
      }
      
    }, (err) => {
      
      console.log(err);
    });
  }

  showUserProfile(){
    this.storage.loadStorage('profile').then((result) => {
      let res:any;
      res = result;
      console.log("profile>>"+result);
      if (result==null){
        this.isSignIn = false;
      }else{
        this.displayname = res.firstname+' '+res.lastname;
        this.email = res.email;
        this.photo = 'data:image/jpeg;base64,' + res.b64Image;
        //this.photo = this.sanitizer.bypassSecurityTrustUrl("data:Image/*;base64,"+res.b64Image);
        this.isSignIn = true;
      }
      
    }, (err) => {
      this.isSignIn = false;
      console.log(err);
    });
  }

  startTimmer(){
    this.subscription = source.subscribe(val => {
      this.timerVal = val;
      if (!this.isDisconnect){
        this.bluetooth.getDustData().then((dust) => {
          if ((Number(dust)>=this.minLev)&&(this.alertDust<this.minLev)){
            this.localNotifications.schedule({
              title: 'AP-DET',
              text: 'Warning! Please WEAR MASK, You are in the unhealthy zone',
              foreground: true
          });
            
          }
          this.alertDust = Number(dust);
          this.geoloation.getLatLon().then((location:any) => {
            this.dustVal = 'PM2.5('+dust+' µg/m3)';
            this.watson.sendWatsoniotData(this.clientId,this.deviceId,dust,location.latitude,location.longitude);
          });
          
        });
      }else{
        console.log('unsubscribe');
        this.subscription.unsubscribe();
      }
      
    });
    
  }

  selectDevice() {
    this.bluetooth.connect(this.connectedDevice).then((result:boolean) => {
      if (result){
        this.listToggle = this.bluetooth.listToggle;
        this.isDisconnect = false;
        this.btn_txt = 'UnPair';
        this.showToast("Successfully  Pair");
        this.startTimmer();
      }else{
        this.isDisconnect = true;
        this.btn_txt = 'Pair';
        this.alertDust=0;
        this.showToast("Successfully UnPair");
      }
    }, (err) => {
      this.isDisconnect = true;
      this.btn_txt = 'Pair';
      this.alertDust=0;
      this.showToast("Successfully UnPair");
    });
  }

  showError(error) {
    alert(JSON.stringify(error));
  }
  
  async showToast(msj) {
    const toast = await this.toastCtrl.create({
      message: msj,
      duration: 1000
    });
    toast.present();

  }

  async getUserProfile(username) {
    console.log("request json >>"+JSON.stringify(this.userReg));
    this.userReg.firstname = username;
    this.restProvider.getUserProfile(this.userReg).then((result) => {
      console.log(result);
      let res : any;
      res = result;
      let userProfile = JSON.parse(res.data);
      if (userProfile.errorMsg){
        this.showToast(userProfile.errorMsg);
      }else{
        this.storage.saveStorage('profile',userProfile.userprofile).then((result:boolean) => {
          if (result){
            this.username = null;
            this.password = null;
            this.userReg = {};
            this.showUserProfile();
            //this.showToast("Successfully Add Profile");
          }
        }, (err) => {
          console.log(err);
        });
      }
      
    }, (err) => {
      console.log(err);
    });
  }

  async logIn() {
    if ( this.username != undefined && this.password != undefined){
      console.log(this.username+"/"+this.password);
      if ( this.username != "" && this.password != ""){
        await this.getUserProfile(this.username);
      }else{
        this.showToast("Username and password is not empty");
      }
      
    }else{
      this.showToast("Username and password is not empty");
    }
    
  }

  async logOut() {
    this.storage.removeStorage('profile').then((result:boolean) => {
      if (result){
        this.showUserProfile();
        //this.showToast("Successfully Add Profile");
      }
    }, (err) => {
      console.log(err);
    });
  }

}
