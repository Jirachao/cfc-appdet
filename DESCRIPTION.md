# Air Pollution Detection Platform [Ap-Det]

## Project Summary:
	Ap-Det is the Pollution Detection Platform, collecting data from IOT sensor into IBM cloud and display through Mobile Application. Ap-Det has unique algorithm used with both stationary sensor and moving sensor that can carried around as key-chain. 
    
    Characteristic:
    1.	Minimum sensor used to collect the data but able to collect large number of data according to the position changed by sensor carrier.
    2.	The hazard alert function for both Sensor carrier and non-sensor user when they exposed to the high concentration of the pollution in certain period of time according to WHO standard. 
    3.	The app includes timer to assist the user on the condition to maintain safe exposure.
    4.	Pollution data will be in detail area. Data is more accurate by the area sensor is on.

## Aim:
1.	To give toxic exposure hazard warning to the, regardless of type*. [In this case we use PM2.5]
2.	To create a platform so that if there is new type of sensor, the developer can use ROM to flash the Micro-Controller device, configure and connect the sensor to cloud. Then they can view the data through the provided Mobile Application.
3.	To utilize the app for both personal and public. The pollution record will be real time where the user is, benefit for both carrier and user around them.

## Background:
    The air pollution, particularly PM2.5, some still unaware. This may because limited knowledge, the awareness of it existence, or does not have the warning system.
    
    Current system in market uses stationary sensor type. It detected only limit area and may not reflect the true data if further away. It does not have the personal real time alert which can be helpful to reduce the exposure.
    
    The detection also specific to the application and sensor that build by the application owner. If there is new type of sensor, it may be proprietary to the new application which might nor reflect true Air Pollution, or the user need to load another app that is not convenience for user awareness.
    We develop the platform that close the gap and gives more benefit to users.

## Innovation: 
1.	Moving sensor collecting data along the way, a warning system for both sensor carrier and non-sensor to give the toxic exposure awareness.
2.	Algorithm to average the pollution data using the peak/office-hour time to average the data
3.	Ready-baked application which user can use to flash the controller, configure and connect to WATSON IOT to display the sensor data in Map

## Technology:

#### Data collection:
1.	Air quality measurement sensor (PMS7003 for this case)
2.	ESP32 PICO Kit to interface the sensor and mobile device using Bluetooth.
3.	Mobile Application (Ionic) to act as a IOT Device interfacing with Watson IOT Platform integrate with Location data
4.	MQTT

#### Data Processing:
1.	IBM WATSON IOT with Cloudant database
2.	IBM Node-Red with DB2 database

#### Presentation:
1.	Mobile Application (Ionic)
2.	Map render using Here Map.

* future road map, current is PM2.5





