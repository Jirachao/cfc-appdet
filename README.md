# CFC Ap-Det

Ap-Det is the Pollution Detection Platform, collecting data from IOT sensor into IBM cloud and display through Mobile Application. Ap-Det has unique algorithm used with both stationary sensor and moving sensor that can carried around as key-chain.

Characteristic:
1.	Minimum sensor used to collect the data but able to collect large number of data according to the position changed by sensor carrier.
2.	The hazard alert function for both Sensor carrier and non-sensor user when they exposed to the high concentration of the pollution in certain period of time according to WHO standard. 
3.	The app includes timer to assist the user on the condition to maintain safe exposure.
4.	Pollution data will be in detail area. Data is more accurate by the area sensor is on.

## The solution at a glance

AP-DET is comprised of 5 different components:
1. [The sensor](SETUP.md#the-sensor)
2. [Mobile application](SETUP.md#mobile-application)
3. [IBM IoT Platform](SETUP.md#ibm-iot-platform)
4. [IBM Cloudant](SETUP.md#ibm-cloudant)
5. [Node-RED](SETUP.md#node-red)

**Architecture** (more detail [here](/image/Architecture.pdf)):
![Architectue](/image/Architecture.png)


**Application flow**
![flow](/image/flow.png)


**Screen flow**
![screeFlow](/image/APDET - Screen Flow.png)

You can find the working sample [here](/bin/ap-det.apk)


## Get Started

* [The Ap-Det story](#the-ap-det-story)
* [Setting up the solution](#setting-up-the-solution)
* [Project Roadmap](#project-roadmap)
* [Resources](#resources) 
* [Authors](#authors)
* [License](#license)

## The Ap-Det story
Watch the video below to understand Ap-Det's solution:

[![](http://img.youtube.com/vi/lOr1gLQFmDY/0.jpg)](https://youtu.be/lOr1gLQFmDY)

## Setting up the solution

See [SETUP.md](SETUP.md)

## Project roadmap

![RoadMap](/image/RoadMap.png)

## Resources

- [HERE Location Services](https://developer.here.com/documentation)
- [IONIC Angular](https://ionicframework.com/)
- [Firebase Cloud Messaging Services](https://firebase.google.com/docs/cloud-messaging)
- [Arduino](https://www.arduino.cc/en/Guide/ArduinoUno).
- [IBM IoT Platform](https://www.ibm.com/internet-of-things/solutions/iot-platform/watson-iot-platform)
- [IBM Cloudant](https://cloud.ibm.com/docs/Cloudant)
- [Node-RED](https://nodered.org/)

## Authors

* Chawaroj Wongphaiboon
* Jiracha Opachaleampun
* Kamnung Pitukkorn
* Mati Bumrungkunakorn
* Warunee Phattarakijvanich

## License

Copyright 2020-2021 Ap-Det

This project is licensed under the Apache 2 License - see the [LICENSE](LICENSE) file for details