# Store data in IBM Cloudant

In this section, you will create a IBM Cloudant service and bind the service to Watson IoT Platform to store the historical data.

## Create an IBM Cloudant DB

1.	Go to the IBM Cloud Catalog and create a new [IBM Cloudant](https://cloud.ibm.com/catalog/services/cloudant)
    - Select a region and choose Lite plan
    - Enter `iot-db` as the service name
    - Select both legacy credentials and IAM as the authentication method and click Create
2.	Go to the Resource list and enter `iot-db` in the Name field to check the status of the service.Once the status changes to Provisioned, click on the service name to see the Manage page.
    - On the left menu, click Service credentials
    - Click New credential and then Add
    - Expand the newly created credentials and save the credentials for future reference

## Create a data connector to store the historical data

Setting up a new connection is a four-step process:

1. Create a service binding that provides Watson IoT Platform with the necessary information to connect to the Cloudant service.
2. Create a connector instance for the service binding.
3. Configure one or more destinations on the connector.
4. Set up one or more forwarding rules for each destination.

To setup a new connection,

1.	On the Watson IoT Platform dashboard, click on the Watson IoT Platform Help icon (menu bar help icon) on the top ribbon, click on API and then View APIs next to Historian Connector to see the interactive API docs.
2.	Under Services, expand the POST /s2s/services endpoint and click Try it out. Replace the placeholders in the JSON below with the cloudant service credentials and use it as content for Example Value under Service body.

	```json	
	{
	   	"name": "iot-cloudant",
	   	"type": "cloudant",
	   	"description": "for store IoT data to support ap-det",
	   	"credentials": {
	    	"username": "CLOUDANT_USERNAME",
	     	"password": "CLOUDANT_PASSWORD",
	     	"host": "CLOUDANT_HOST",
	     	"port": 443,
	     	"url": "CLOUDANT_URL"
	   	}
	}
	```

3.	Click Execute to see HTTP 201 response. Save the id (serviceID) from the response for the next API call.
4.	Under HistorianConnectors, expand the POST /historianconnectors endpoint and click Try it out. Replace the Example Value under Connector body with the JSON below. Don't forget to replace the SERVICE_ID with the id from the response above.

	```json
	{
		"name": "iot-cloudant-connector",
	   	"description": "Historian connector connecting IoT platform to cloudant",
	   	"serviceId": "SERVICE_ID",
	   	"type": "cloudant",
	   	"enabled": true
	}
	```

5.	Click Execute to see HTTP 201 response. Save the id(connectorID) from the response for future reference.
6.	Under Destinations, expand the POST /historianconnectors/{connectorId}/destinations endpoint and click Try it out. Provide the id(connectorID) and replace the Example Value under Destination body with the JSON below.

	```json
	{
		"name": "default",
	 	"type": "cloudant",
	 	"configuration": {
	   		"bucketInterval": "MONTH"
	  	}
	}
	```

7.	Click Execute to see HTTP 201 response.
8.	Under Forwarding Rules, expand POST /historianconnectors/{connectorId}/forwardingrules endpoint and click Try it out. Provide the id(connectorID) and replace the Example Value under Forwarding Rule body with the JSON below.

	```json
	{
	 	"name": "iot-cloudant-rule",
	 	"destinationName": "default",
		"type": "event",
	 	"selector": {
	   		"deviceType": "iotsensor_device",
	   		"eventId": "sensorData"
	  	}
	}
	```
     
9.	Click Execute to see HTTP 201 response.

## Create custom databases in IBM Cloudant

After finish step above, you will create databases follow name list below in your IBM Cloudant to store the master data & configuration data for support our Node-RED app.

1. apdet-config
2. apdet-device-db
3. apdet-pmlevel-range-master
4. apdet-user-profile
5. apdet_pollution_report

![ap-det-cloudant_db](/image/ap-det-cloudant-db.png)

## Create intial data in databases

For the your new project, you must to setup master & configuration data in your custom databases above for our application with api in flows list below.

1. CreatePMLevelRangeMasterFlow : api for setup master data in `apdet-pmlevel-range-master` database, you can use request in `node-red/pmlevelrangemaster.json` file
2. CreateConfigDataFlow : api for setup configuration data in `apdet-config` database, you can useuse request in `node-red/config.json` file)

