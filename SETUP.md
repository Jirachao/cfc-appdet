# Setup

The application using IONIC Angular as front end and Node-RED run as back end. The application via bluetooth connect with senser device for get dust data and integrates map and location services provided by HERE Technologies. Use the following steps to get setup up and running. 

## Software Prerequisites

- Install [Arduino Software (IDE)](https://www.arduino.cc/en/Guide/ArduinoUno).
- Install [Ionic CLI dependencies](https://ionicframework.com/getting-started).
    - [Node.js](https://nodejs.org/en/)
    - **iOS only**
        - [Xcode](https://itunes.apple.com/us/app/xcode/id497799835?mt=12)
    - **Android only**
        - [Java Development Kit](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
        - [Android Studio](https://developer.android.com/studio/index.html) - add Android 9 (Pie) SDK & configure `ANDROID_HOME`
- Clone the [repository](https://gitlab.com/Jirachao/cfc-appdet).
- Register for a [HERE](https://developer.here.com/ref/) account.
- Register project in a [Firebase](https://console.firebase.google.com/) console.
- Register for a [IBM Cloud](https://cloud.ibm.com/docs/account?topic=account-signup) account.

## Hardware Prerequisites

- SENSOR Module [Laser PM2.5 PMS7003](https://www.amazon.com/PMS7003-High-Precision-Concentration-Digital-Particles/dp/B085X1VCQR)
- Development Board [ESP32 PICO KIT](https://www.amazon.com/DIYmall-ESP32-PICO-KIT-Development-Board-ESP-32/dp/B00RSPTHE0)
- Breadboards [Breadboards](https://www.amazon.com/Breadboard-Solderless-Prototype-PCB-Board/dp/B077DN2PS1/ref=sr_1_14?dchild=1&keywords=Breadboards&qid=1591935495&sr=8-14)
- Jumper Wires [Jumper Wires](https://www.amazon.com/EDGELEC-Breadboard-Optional-Assorted-Multicolored/dp/B07GD2BWPY/ref=pd_bxgy_3/130-4902587-3410210?_encoding=UTF8&pd_rd_i=B07GD2BWPY&pd_rd_r=6b5952c3-547a-4883-90b1-cf8683df6bb6&pd_rd_w=FdaLj&pd_rd_wg=658TX&pf_rd_p=4e3f7fc3-00c8-46a6-a4db-8457e6319578&pf_rd_r=5PAJ607VHR1JTAXV0GW6&psc=1&refRID=5PAJ607VHR1JTAXV0GW6)
- 5V Battery or connect standard Powerbank to MicroUSB Port

## The sensor

To upload arduino code to development board for get dust data via bluetooth:

![schementic](/image/pm25_board.png)

1. Open Arduino IDE:
    1. Connect USB with ESP32 development board and see COM Port number.
    2. Go to the `arduino/esp32_PMS7003` directory of the cloned repo.
    3. Open file `esp32_PMS7003.ino` with Arduino IDE.
    4. Select correct COM Port and ESP32 Board.
    5. Select upload and wait for succesful.
    6. Connect Sensor Device with 5V Battery or Powered through Micro USB Port.

## Mobile application

### Generate an API Key from the HERE Developer Portal

The application uses HERE Location Services for maps, searching, and routing.

To access these services, an API Key is required. Follow the instructions outlined in the [HERE Developer Portal](https://developer.here.com/ref/IBM_starterkit_Disasters2020?create=Freemium-Basic) to [generate a JavaScript API Key](https://developer.here.com/documentation/authentication/dev_guide/topics/api-key-credentials.html).

### Create project in the Firebase console

The application uses Firebase Cloud Messaging Services for notification, alert to unhealthy area.

To access these services, create project. Follow the instructions outlined in the [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging) and get `google-services.json` in to project.

### Run the mobile application

To run the mobile application (using the Xcode iOS Simulator or Android Studio Emulator) or Real device:

1. From a terminal:
    1. Go to the `cfc-appdet` directory.
    2. Copy the `google-services.json` file in the `cfc-appdet` directory.
    3. Install the dependencies: `npm install`.
    4. **iOS only**: Create platform with: `ionic cordova platform add ios`.
    5. **Android only**: Create platform with: `ionic cordova platform add android`.
    6. Launch the app in the [simulator/emulator](https://ionicframework.com/docs/cli/commands/cordova-emulate)
    7. Launch the app in the real device with build:
        - **iOS**: [Build](https://ionicframework.com/docs/cli/commands/cordova-build) *.ipa: `ionic cordova build ios`
        - **Android**: [Build](https://ionicframework.com/docs/cli/commands/cordova-build) *.apk: `ionic cordova build android`

## IBM IoT Platform

This is a very straight forward step, simply register the new devices and make sure they connect to the platform, you can [create](/IBM_IoT_Readme.md) IBM IoT Platform.

## IBM Cloudant

IBM Watson IoT Platform use IBM Cloudant service to store the historical data, instruction for IBM Cloudant can be found [here](/IBM_Cloudant_Readme.md)

## Node-RED

We create api services to analyze pollution data and manage devices to support our mobile application, you can [create](https://developer.ibm.com/components/node-red/tutorials/how-to-create-a-node-red-starter-application/) Node-RED Starter application, you can find [the code here](/node-red/flows.json), if you want to import to your personal project, just take into account that credentials, tokens and sensitive data are deleted. 

For our node-red application, we have to add more dependencies follow list below, you can add to your `package.json` file after created node-red starter application.

```json
"node-red-contrib-cloudantplus-selector":"0.x"
"node-red-contrib-parallel-iterator":"0.x"
"node-red-contrib-push":"0.x"
"node-red-contrib-config":"0.x"
```

![package_json](/image/package_json.png)

So, lets see flow description in below:-

- InitialConfigValueFlow: this flow used for initialization global value and will be applied at startup before any flows are started

![intitalConfigValue](/image/IntitalConfigValue.png)

- RegisterDeviceFlow: api for register mobile & sensor device to our application.

![regDeviceFlow](/image/RegisterDeviceFlow.png)

- UpdateCurrentPolFlow: api for get pollution data for current location

![updateCurrPolFlow](/image/UpdateCurrentPolFlow.png)
	
- GetPolDataFlow: api for get all pollution data to support mobile application.

![getPolDataFlow](/image/GetPolDataFlow.png)

- ReportPolSourceFlow: api for save pollution report.

![reportPolSrcFlow](/image/ReportPolSourceFlow.png)
	
- GetUserProfileFlow: api for get user profile information.

![getUsrProfFlow](/image/GetUserProfileFlow.png)

- SaveUserProfileFlow: api for save user profile information.

![saveUsrProfFlow](/image/SaveUserProfileFlow.png)

- GetPolDataForPublicFlow: api to support public to get our all pollution data.

![GetPolDataForPubFlow](/image/GetPolDataForPublicFlow.png)

- CreatePMLevelRangeMasterFlow : api for setup master data.

![CreatePMLevelRangeMasterFlow](/image/CreatePMLevelRangeMasterFlow.png)

- CreateConfigDataFlow : api for setup configuration data.

![CreateConfigDataFlow](/image/CreateConfigDataFlow.png)

