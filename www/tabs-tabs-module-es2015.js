(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-tabs>\n\n    <ion-tab-bar class=\"tab-box\" slot=\"bottom\">\n        <ion-tab-button tab=\"countryMapTab\">\n            <img class=\"icon-menu\" src=\"../../assets/icon/earth.png\">\n        </ion-tab-button>\n\n        <ion-tab-button tab=\"KnowledgeTab\">\n            <img class=\"icon-menu\" src=\"../../../assets/icon/book-information-button.png\">\n        </ion-tab-button>\n\n        <ion-tab-button tab=\"yourLocationTab\" actions>\n            <img class=\"icon-menu\" src=\"../../../assets/icon/location-mark.png\">\n        </ion-tab-button>\n\n        <ion-tab-button tab=\"ReportTab\">\n            <img class=\"icon-menu\" src=\"../../../assets/icon/report-service.png\">\n        </ion-tab-button>\n\n        <ion-tab-button tab=\"myprofile\">\n            <img class=\"icon-menu\" src=\"../../../assets/icon/person-profile.png\">\n        </ion-tab-button>\n    </ion-tab-bar>\n\n</ion-tabs>");

/***/ }),

/***/ "./src/app/tabs/tabs-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tabs/tabs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");




const routes = [
    {
        path: 'tabs',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'countryMapTab',
                loadChildren: () => Promise.all(/*! import() | countryMapTab-countryMapTab-module */[__webpack_require__.e("default~KnowledgeTab-KnowledgeTab-module~ReportTab-ReportTab-module~countryMapTab-countryMapTab-modu~15b868f8"), __webpack_require__.e("countryMapTab-countryMapTab-module")]).then(__webpack_require__.bind(null, /*! ../countryMapTab/countryMapTab.module */ "./src/app/countryMapTab/countryMapTab.module.ts")).then(m => m.countryMapTabPageModule)
            },
            {
                path: 'KnowledgeTab',
                loadChildren: () => Promise.all(/*! import() | KnowledgeTab-KnowledgeTab-module */[__webpack_require__.e("default~KnowledgeTab-KnowledgeTab-module~ReportTab-ReportTab-module~countryMapTab-countryMapTab-modu~15b868f8"), __webpack_require__.e("KnowledgeTab-KnowledgeTab-module")]).then(__webpack_require__.bind(null, /*! ../KnowledgeTab/KnowledgeTab.module */ "./src/app/KnowledgeTab/KnowledgeTab.module.ts")).then(m => m.KnowledgeTabPageModule)
            },
            {
                path: 'yourLocationTab',
                loadChildren: () => Promise.all(/*! import() | your-location-tab-your-location-tab-module */[__webpack_require__.e("default~KnowledgeTab-KnowledgeTab-module~ReportTab-ReportTab-module~countryMapTab-countryMapTab-modu~15b868f8"), __webpack_require__.e("your-location-tab-your-location-tab-module")]).then(__webpack_require__.bind(null, /*! ../your-location-tab/your-location-tab.module */ "./src/app/your-location-tab/your-location-tab.module.ts")).then(m => m.YourLocationTabPageModule)
            },
            {
                path: 'ReportTab',
                loadChildren: () => Promise.all(/*! import() | ReportTab-ReportTab-module */[__webpack_require__.e("default~KnowledgeTab-KnowledgeTab-module~ReportTab-ReportTab-module~countryMapTab-countryMapTab-modu~15b868f8"), __webpack_require__.e("ReportTab-ReportTab-module")]).then(__webpack_require__.bind(null, /*! ../ReportTab/ReportTab.module */ "./src/app/ReportTab/ReportTab.module.ts")).then(m => m.ReportTabPageModule)
            },
            {
                path: 'myprofile',
                loadChildren: () => __webpack_require__.e(/*! import() | myprofile-myprofile-module */ "myprofile-myprofile-module").then(__webpack_require__.bind(null, /*! ../myprofile/myprofile.module */ "./src/app/myprofile/myprofile.module.ts")).then(m => m.MyprofilePageModule)
            },
            {
                path: '',
                redirectTo: '/tabs/yourLocationTab',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/yourLocationTab',
        pathMatch: 'full'
    }
];
let TabsPageRoutingModule = class TabsPageRoutingModule {
};
TabsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], TabsPageRoutingModule);



/***/ }),

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs-routing.module */ "./src/app/tabs/tabs-routing.module.ts");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");







let TabsPageModule = class TabsPageModule {
};
TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
        ],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
    })
], TabsPageModule);



/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".icon-menu {\n  width: 30px;\n}\n\n.tab-box :hover,\n.tab-box :active,\n.tab-box :focus {\n  background: #6094e2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9qaXJhY2hhby9UQkFOSyBUZWFtIERvYy9wbTI1L2NmYy1hcHBkZXQvc3JjL2FwcC90YWJzL3RhYnMucGFnZS5zY3NzIiwic3JjL2FwcC90YWJzL3RhYnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQ0NKOztBREdLOzs7RUFHRyxtQkFBQTtBQ0FSIiwiZmlsZSI6InNyYy9hcHAvdGFicy90YWJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pY29uLW1lbnUge1xuICAgIHdpZHRoOiAzMHB4O1xufVxuXG4udGFiLWJveCB7XG4gICAgIDpob3ZlcixcbiAgICAgOmFjdGl2ZSxcbiAgICA6Zm9jdXMge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjNjA5NGUyO1xuICAgIH1cbn0iLCIuaWNvbi1tZW51IHtcbiAgd2lkdGg6IDMwcHg7XG59XG5cbi50YWItYm94IDpob3Zlcixcbi50YWItYm94IDphY3RpdmUsXG4udGFiLWJveCA6Zm9jdXMge1xuICBiYWNrZ3JvdW5kOiAjNjA5NGUyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TabsPage = class TabsPage {
    constructor() { }
};
TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tabs',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TabsPage);



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es2015.js.map