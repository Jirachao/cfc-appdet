# Create IBM IoT Platform

In this section, you will create Internet of Things Platform service - The hub which can manage devices, securely connect and collect data, and make historical data available for our  applications.

1.	Go to the IBM Cloud Catalog and select [Internet of Things Platform](https://cloud.ibm.com/catalog/services/internet-of-things-platform) under the Internet of Things section.
2.	Select a region and choose Lite as the pricing plan.
3.	Enter `IoT Ap-Det` as the service name.
4.	Click Create and then Launch(under Manage) the dashboard.
5.	From the side menu, select Security > click Edit icon next to Connection Security and choose TLS Optional under Default Rule > Security Level and click Save.
6.	From the side menu, select Devices > Device Types and Add Device Type.
7.	Enter `iotsensor_device` as the Name and click Next and Finish.
8.	Next, click on Register Devices.
9.	Select `iotsensor_device` as Device Type and enter `iotsensor` for Device ID.
10.	Click Next until the Security screen is displayed.
11.	Enter a value for the Authentication Token, for example: `myauthtoken` and click Next.
12.	After clicking Finish, your connection information is displayed. Keep this tab open.
